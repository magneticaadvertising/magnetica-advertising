/**
 * Add to Exclude Lists all placements that have High CTRs (>50%) and Low Impressions (<10) without Converted Clicks 
 * Before the excluding, checks if the placement is included in a WhiteList
 * @author Jordi A. Sintes
 */

// -------------------------------------------------------

  var time_MediumCTRHighClicks = 'ALL_TIME';
  var time_HighCTRLowImpressions = 'ALL_TIME';

  var excludeList_MediumCTRHighClicks = 'MediumCTRHighClicks';
  var excludeList_HighCTRLowImpressions = 'HighCTRLowImpressions';
 
  var excludeList_Quarantine = 'Quarantine';
  var whiteList = 'WhitelistPlacements';

  var emailRecipient = "odena@magneticaadvertising.com";
  var client = "1892911913";
 


//--------------------------------------------------------
function inWhiteList(domain){
  var WhiteListPlacementListIterator = 
  AdWordsApp.excludedPlacementLists()
   .withCondition('Name =' + whiteList)
   .get();   
  
  if (WhiteListPlacementListIterator.hasNext()) {
    var WhiteListPlacementList = WhiteListPlacementListIterator.next();
    var WhiteListPlacementListItems = 
    WhiteListPlacementList.excludedPlacements()
    .get();
    
    while (WhiteListPlacementListItems.hasNext()) {
      var WhiteListPlacementListItem = WhiteListPlacementListItems.next();
      if (WhiteListPlacementListItem.getUrl()== domain) {
       return true
      }
    }
  }
}
function isQuarantine(domain){
  var placementSelector = AdWordsApp.display().placements()
  .withCondition("PlacementUrl = \"" + domain +"\"")
  .withCondition("ConvertedClicks > 0")
  .get();
  
  if (placementSelector.totalNumEntities() > 0) {
    return true;
  }
}

function excludePlacement(list, domain){

  if (isQuarantine(domain)) {
    var quarantineList = getOrCreateExcludeList(excludeList_Quarantine);
    quarantineList.addExcludedPlacement(domain);
    return true;
  }
  else{
    var excludeList = getOrCreateExcludeList(list);
    excludeList.addExcludedPlacement(domain);
    return true;
  }
}

function existExcludeList(list){
  var excludedPlacementListIterator =
   AdWordsApp.excludedPlacementLists()
   .withCondition('Name = ' + list)
   .get();

   if (excludedPlacementListIterator.totalNumEntities() == 1) {
      return true;
   }
}

function createExcludeList(list){
  var excludedPlacementListOperation =
    AdWordsApp.newExcludedPlacementListBuilder()
      .withName(list)
      .build();

    if (excludedPlacementListOperation.isSuccessful()) {
      return excludedPlacementListOperation.getResult();
    }
}

function getOrCreateExcludeList(list){

  if (!existExcludeList(list)) {
    return createExcludeList(list);
  }
    else{
    var excludedPlacementListIterator =
     AdWordsApp.excludedPlacementLists()
     .withCondition('Name = ' + list)
     .get();
    
    while (excludedPlacementListIterator.hasNext()) {
      var excludedPlacementList = excludedPlacementListIterator.next();
      return excludedPlacementList;
    }   
  }
}

function sendQuarantineMail(list, placements){

  var message = "Lista de Placements sujetos a Quarantine por " + list + " para el cliente: " + client ;
  var subject = list + " Quarantine Report: "+ client; 

  placements.forEach(function(entry) {
    message = message + "\n" + entry;
  });

  message = message + "\n\n Saludos;";
  MailApp.sendEmail(emailRecipient, subject, message);  
}

function HighCTRLowImpressions () {
  var placementIterator = AdWordsApp.display().placements()
  .withCondition("CampaignStatus != REMOVED")
  .withCondition("CampaignName DOES_NOT_CONTAIN 'RMK'")
  .withCondition("Ctr > 0.5")
  .withCondition("Impressions < 10")
  .withCondition("ConvertedClicks < 1")
  .forDateRange(time_HighCTRLowImpressions)
  .get();

  var quarantinePlacements = new Array();

  while (placementIterator.hasNext()) {
    var placement = placementIterator.next();
    var placementUrl = placement.getUrl();

    var stats = placement.getStatsFor(time_HighCTRLowImpressions);
    var ctr = stats.getCtr();
    var impressions = stats.getImpressions();
      
    if (!inWhiteList(placementUrl)) {
      if(excludePlacement(excludeList_HighCTRLowImpressions, placementUrl)){        
        Logger.log("Excluded by HIGH CTR ("+ ctr*100 +") - LOW IMPRESSIONS ("+ impressions +"): " + placementUrl); 
        if (isQuarantine(placementUrl)) {
          quarantinePlacements.push(placementUrl);
        }
      }
      else{
        Logger.log("!!!!!ERROR!!!!! excluding by HIGH CTR ("+ ctr*100 +") - LOW IMPRESSIONS ("+ impressions +"): " + placementUrl);
      }
    }
    else{   
      Logger.log("NOT Excluded (in WhiteList): " + placementUrl); 
    }      
  }

  sendQuarantineMail(excludeList_HighCTRLowImpressions, quarantinePlacements);
}

function run () {
  HighCTRLowImpressions();
}

function main () {
  run();
}