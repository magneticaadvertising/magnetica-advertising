/**
 * Add to Exclude Lists all placements that have High CTRs (>50%) and Low Impressions (<10) without Converted Clicks 
 * Before the excluding, checks if the placement is included in a WhiteList
 * @author Jordi A. Sintes www.jordisintes.com
 */

// -------------------------------------------------------

  //Lapso de tiempo que el Script Revisará
  //Opciones: (TODAY, YESTERDAY, LAST_7_DAYS, THIS_WEEK_SUN_TODAY, LAST_WEEK, LAST_14_DAYS, LAST_30_DAYS, LAST_BUSINESS_WEEK, LAST_WEEK_SUN_SAT, THIS_MONTH, LAST_MONTH, ALL_TIME.)
  var scriptDateRange = 'ALL_TIME';

  //Nombre de la lista de exclusión, si no está creada el script la creará
  var scriptExcludeList = 'MediumCTRHighClicks';
 
  //Nombre de la lista de cuarentena, si no está creada el script la creará
  var excludeList_Quarantine = 'Quarantine';

  //Nombre de la lista blanca, si no está creada el script NO la creará
  var whiteList = 'WhitelistPlacements';

  //Correo Electrónico que recibirá la lista de cuarentena.
  var emailRecipient = "odena@magneticaadvertising.com";

  //Nombre del cliente/cuenta, aparecerá en el correo
  var client = "1892911913";
 


//--------------------------------------------------------

function main () {
  MediumCTRHighClicks();
}

function MediumCTRHighClicks() {
  var placementIterator = AdWordsApp.display().placements()
  .withCondition("CampaignStatus != REMOVED")
  .withCondition("CampaignName DOES_NOT_CONTAIN 'RMK'")
  .withCondition("Ctr > 0.08")
  .withCondition("Clicks > 20")
  .withCondition("ConvertedClicks < 1")
  .forDateRange(scriptDateRange)
  .get();

  var quarantinePlacements = new Array();

  while (placementIterator.hasNext()) {
    var placement = placementIterator.next();
    var placementUrl = placement.getUrl();    
          
    if (!inWhiteList(placementUrl)) {
      var stats = placement.getStatsFor(scriptDateRange);
      var ctr = stats.getCtr();
      var clicks = stats.getClicks();

      if(excludePlacement(scriptExcludeList, placementUrl)){
        Logger.log("Excluded by MEDIUM CTR ("+ ctr*100 +") - HIGH CLICKS ("+ clicks +"): " + placementUrl);
        if (isQuarantine(placementUrl)) {
          quarantinePlacements.push(placementUrl);
        }
      }
      else{
        Logger.log("!!!!!ERROR!!!!! excluding by MEDIUM CTR ("+ ctr*100 +") - HIGH CLICKS ("+ clicks +"): " + placementUrl);
      }
    }
    else{   
      Logger.log("NOT Excluded (in WhiteList): " + placementUrl); 
    }      
  }

  if (quarantinePlacements.length > 0){
    sendQuarantineMail(scriptExcludeList, quarantinePlacements);
  }
}

function inWhiteList(domain){
  var WhiteListPlacementListIterator = 
  AdWordsApp.excludedPlacementLists()
   .withCondition('Name =' + whiteList)
   .get();   
  
  if (WhiteListPlacementListIterator.hasNext()) {
    var WhiteListPlacementList = WhiteListPlacementListIterator.next();
    var WhiteListPlacementListItems = 
    WhiteListPlacementList.excludedPlacements()
    .get();
    
    while (WhiteListPlacementListItems.hasNext()) {
      var WhiteListPlacementListItem = WhiteListPlacementListItems.next();
      if (WhiteListPlacementListItem.getUrl()== domain) {
       return true
      }
    }
  }
}

function excludePlacement(list, domain){

  if (isQuarantine(domain)) {
    var quarantineList = getOrCreateExcludeList(excludeList_Quarantine);
    quarantineList.addExcludedPlacement(domain);
    return true;
  }
  else{
    var excludeList = getOrCreateExcludeList(list);
    excludeList.addExcludedPlacement(domain);
    return true;
  }
}

function isQuarantine(domain){
  var placementSelector = AdWordsApp.display().placements()
  .withCondition("PlacementUrl = \"" + domain +"\"")
  .withCondition("ConvertedClicks > 0")
  .get();
  
  if (placementSelector.totalNumEntities() > 0) {
    return true;
  }
}

function getOrCreateExcludeList(list){

  if (!existExcludeList(list)) {
    return createExcludeList(list);
  }
    else{
    var excludedPlacementListIterator =
     AdWordsApp.excludedPlacementLists()
     .withCondition('Name = ' + list)
     .get();
    
    while (excludedPlacementListIterator.hasNext()) {
      var excludedPlacementList = excludedPlacementListIterator.next();
      return excludedPlacementList;
    }   
  }
}

function existExcludeList(list){
  var excludedPlacementListIterator =
   AdWordsApp.excludedPlacementLists()
   .withCondition('Name = ' + list)
   .get();

   if (excludedPlacementListIterator.totalNumEntities() == 1) {
      return true;
   }
}

function createExcludeList(list){
  var excludedPlacementListOperation =
    AdWordsApp.newExcludedPlacementListBuilder()
      .withName(list)
      .build();

    if (excludedPlacementListOperation.isSuccessful()) {
      return excludedPlacementListOperation.getResult();
    }
}

function sendQuarantineMail(list, placements){

  var message = "Lista de Placements sujetos a Quarantine por " + list + " para el cliente: " + client ;
  var subject = list + " Quarantine Report: "+ client; 

  placements.forEach(function(entry) {
    message = message + "\n" + entry;
  });

  message = message + "\n\n Saludos;";
  MailApp.sendEmail(emailRecipient, subject, message);  
}