/**
 * Add to Exclude Lists all placements that have TLDs.
 * @author Jordi A. Sintes
 */

// Top Level Domains to exclude

var TLDs = '.xyz, .tk, .download, .sexy, .info, .top, .ovh, .ro, .tips, .am';
var dateRange = 'ALL_TIME';
var placementList = 'removePlacementByDomain';
var whiteList = 'WhitelistPlacements';

// -------------------------------------------------------

function inWhiteList(domain){
  var WhiteListPlacementListIterator = 
  AdWordsApp.excludedPlacementLists()
   .withCondition('Name =' + whiteList)
   .get();
  
  if (WhiteListPlacementListIterator.hasNext()) {
    var WhiteListPlacementList = WhiteListPlacementListIterator.next();
    var WhiteListPlacementListItems = 
    WhiteListPlacementList.excludedPlacements()
    .get();
    
    while (WhiteListPlacementListItems.hasNext()) {
      var WhiteListPlacementListItem = WhiteListPlacementListItems.next();
      if (WhiteListPlacementListItem.getUrl()== domain) {
       return true
      }
    }
  }
}

function addPlacementToExcludedList(list, domain){
   var excludedPlacementListIterator =
   AdWordsApp.excludedPlacementLists()
   .withCondition('Name = ' + list)
   .get();

  if (excludedPlacementListIterator.totalNumEntities() == 1) {
    var excludedPlacementList = excludedPlacementListIterator.next();
    excludedPlacementList.addExcludedPlacement(domain);
    return true;
  }  
  else{
    var excludedPlacementListOperation =
    AdWordsApp.newExcludedPlacementListBuilder()
      .withName(list)
      .build();

    if (excludedPlacementListOperation.isSuccessful()) {
      var excludedPlacementList = excludedPlacementListOperation.getResult();
      excludedPlacementList.addExcludedPlacement(domain);
      return  true;
    }
  }
}

function removePlacementByDomain (domain) {
  var placementSelector = AdWordsApp.display().placements()
  .withCondition("PlacementUrl CONTAINS '" + domain + "'")  
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.com' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.es' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.org' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.net' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.cat' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.co.uk' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.it' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.de' ")
  .withCondition("PlacementUrl DOES_NOT_CONTAIN '.fr' ")
  .withCondition("CampaignStatus != REMOVED")
  .forDateRange(dateRange);

  var placementIterator = placementSelector.get();

  while (placementIterator.hasNext()) {
    var placement = placementIterator.next();
    var placementUrl = placement.getUrl();
      
    if (!inWhiteList(placementUrl)) {
      if(addPlacementToExcludedList(placementList, placementUrl)){
        Logger.log("Excluded by SUSPECT DOMAIN: " + placementUrl);
      }
      else{
        Logger.log("!!!!!ERROR!!!!! excluding by SUSPECT DOMAIN: " + placementUrl);
      }
    }
    else{   
      Logger.log("NOT Excluded (in WhiteList): " + placementUrl); 
    }      
  }
}

function run () {
  TLDs.split(',').map(function (tld) {
    return tld.trim();
  }).forEach(function (domain) {
    removePlacementByDomain(domain);
  });
}

function main () {
  run();
}