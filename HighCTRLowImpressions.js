/**
 * Add to Exclude Lists all placements that have High CTRs (>50%) and Low Impressions (<10) without Converted Clicks 
 * Before the excluding, checks if the placement is included in a WhiteList
 * @author Jordi A. Sintes --> www.jordisintes.com <--
 */

// -------------------------------------------------------

  //Lapso de tiempo que el Script Revisará
  //Opciones: (TODAY, YESTERDAY, LAST_7_DAYS, THIS_WEEK_SUN_TODAY, LAST_WEEK, LAST_14_DAYS, LAST_30_DAYS, LAST_BUSINESS_WEEK, LAST_WEEK_SUN_SAT, THIS_MONTH, LAST_MONTH, ALL_TIME.)
  var scriptDateRange = 'ALL_TIME';
  //Nombre de la lista de exclusión, si no está creada el script la creará
  var scriptExcludeList = 'HighCTRLowImpressions';
  //Nombre de la lista de cuarentena, si no está creada el script la creará
  var excludeList_Quarantine = 'Quarantine';
  //Nombre de la lista blanca, si no está creada el script NO la creará
  var whiteList = 'WhitelistPlacements';
  //Correo Electrónico que recibirá la lista de cuarentena.
  var emailRecipient = "odena@magneticaadvertising.com";
  //Nombre del cliente/cuenta, aparecerá en el correo
  var client = "1892911913";

//--------------------------------------------------------

//Empieza
function main () {
  HighCTRLowImpressions();
}

//Filtramos los placements que queremos excluir segun ciertos criterios y en el periodo de tiempo anteriormente acordado
function HighCTRLowImpressions () {
  var placementIterator = AdWordsApp.display().placements()
  .withCondition("CampaignStatus != REMOVED")
  .withCondition("CampaignName DOES_NOT_CONTAIN 'RMK'")
  .withCondition("Ctr > 0.5")
  .withCondition("Impressions < 10")
  .withCondition("ConvertedClicks < 1")
  .forDateRange(scriptDateRange)
  .get();

  var quarantinePlacements = new Array();

  //Una vez hemos obtenido los placements que encajan con nuestros criterios los recorremos uno a uno
  while (placementIterator.hasNext()) {
    var placement = placementIterator.next();
    var placementUrl = placement.getUrl();

    //Comprovamos que el placement no está en la WhiteList
    if (!inWhiteList(placementUrl)) {
      //Cargamos los datos del placement para mostrarlos en el Log
      var stats = placement.getStatsFor(scriptDateRange);
      var ctr = stats.getCtr();
      var impressions = stats.getImpressions();

      //Excluimos el placement
      if(excludePlacement(placementUrl)){        
        Logger.log("Excluded by HIGH CTR ("+ ctr*100 +") - LOW IMPRESSIONS ("+ impressions +"): " + placementUrl); 
        //Si esta en cuarentena lo adjuntamos al array que nos servira para mandar el correo de placements "sospechosos"
        if (isQuarantine(placementUrl)) {
          quarantinePlacements.push(placementUrl);
        }
      }
      else{
        Logger.log("!!!!!ERROR!!!!! excluding by HIGH CTR ("+ ctr*100 +") - LOW IMPRESSIONS ("+ impressions +"): " + placementUrl);
      }
    }
    else{   
      Logger.log("NOT Excluded (in WhiteList): " + placementUrl); 
    }      
  }

  if (quarantinePlacements.length > 0){
    sendQuarantineMail(scriptExcludeList, quarantinePlacements);
  }
}

//Toma la lista segun su nombre y si la encuentra comprueba que el placement especifico esta en la lista.
function inWhiteList(domain){
  var WhiteListPlacementListIterator = 
  AdWordsApp.excludedPlacementLists()
   .withCondition('Name =' + whiteList)
   .get();   
  
  if (WhiteListPlacementListIterator.hasNext()) {
    var WhiteListPlacementList = WhiteListPlacementListIterator.next();
    var WhiteListPlacementListItems = 
    WhiteListPlacementList.excludedPlacements()
    .get();
    
    while (WhiteListPlacementListItems.hasNext()) {
      var WhiteListPlacementListItem = WhiteListPlacementListItems.next();
      if (WhiteListPlacementListItem.getUrl()== domain) {
       return true
      }
    }
  }
}

//Excluye el placement definitivamente o si es sospechoso lo pone en cuarentena
function excludePlacement(domain){

  var excludeList;

  if (isQuarantine(domain)) {
    excludeList = getOrCreateExcludeList(excludeList_Quarantine);    
  }
  else{
    excludeList = getOrCreateExcludeList(scriptExcludeList);
  }

  excludeList.addExcludedPlacement(domain);
  return true;
}

//Comprueba si el placement es sospechoso, en este caso considera sospechoso el placement 
// si cumpliendo el filtro inicial ha convertido en algun otro grupo de anuncios distinto al cual ha sido detectado.
function isQuarantine(domain){
  var placementSelector = AdWordsApp.display().placements()
  .withCondition("PlacementUrl = \"" + domain +"\"")
  .withCondition("ConvertedClicks > 0")
  .get();
  
  if (placementSelector.totalNumEntities() > 0) {
    return true;
  }
}

//Retorna o crea y retorna una lista de exclusión
function getOrCreateExcludeList(list){

  if (!existExcludeList(list)) {
    return createExcludeList(list);
  }
    else{
    var excludedPlacementListIterator =
     AdWordsApp.excludedPlacementLists()
     .withCondition('Name = ' + list)
     .get();
    
    while (excludedPlacementListIterator.hasNext()) {
      var excludedPlacementList = excludedPlacementListIterator.next();
      return excludedPlacementList;
    }   
  }
}

//Comprueba si la lista de exclusión existe
function existExcludeList(list){
  var excludedPlacementListIterator =
   AdWordsApp.excludedPlacementLists()
   .withCondition('Name = ' + list)
   .get();

   if (excludedPlacementListIterator.totalNumEntities() == 1) {
      return true;
   }
}

//Crea la lista de exclusión
function createExcludeList(list){
  var excludedPlacementListOperation =
    AdWordsApp.newExcludedPlacementListBuilder()
      .withName(list)
      .build();

    if (excludedPlacementListOperation.isSuccessful()) {
      return excludedPlacementListOperation.getResult();
    }
}

//Envia un correo al destinatario especificado al principio con los placements que han ido a cuarentena
function sendQuarantineMail(placements){

  var message = "Lista de Placements sujetos a Quarantine por " + scriptExcludeList + " para el cliente: " + client ;
  var subject = list + " Quarantine Report: "+ client; 

  placements.forEach(function(entry) {
    message = message + "\n" + entry;
  });

  message = message + "\n\n Saludos;";
  MailApp.sendEmail(emailRecipient, subject, message);  
}